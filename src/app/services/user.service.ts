import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClientsData } from '../models/clients-data';
import { CurrentUser } from '../models/current-user';
import { TokenStorageService } from './token-storage.service';

const baseUrl = 'http://localhost:8000/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  get(): Observable<CurrentUser> {
    let params = new HttpParams();
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.get(`${baseUrl}`,{
      responseType: 'json',
      headers: header,
      params
    });
  }

  getCurrentRole(){
    return this.tokenStorage.getRole();
  }

  connectToSeller(sellerCode: any): Observable<any> {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.post(`${baseUrl}/connectaccounts`,{
      responseType: 'json',
      headers: header,
      params: new HttpParams().set('seller_code', sellerCode)
    });
  }

  
  update( data: any): Observable<any> {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };
    
    return this.http.put(`${baseUrl}/updateuserdata`, data,{
      responseType: 'json',
      headers: header
    });
  }


  getClientsList(): Observable<ClientsData[]> {
    let params = new HttpParams();
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.get<ClientsData[]>(`${baseUrl}/getclients`,{
      responseType: 'json',
      headers: header,
      params
    });
  }
}
