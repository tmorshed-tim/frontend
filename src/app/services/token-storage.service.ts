import { Injectable } from '@angular/core';
import jwt_decode from "jwt-decode";


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));

    let tokenInfo = this.getDecodedAccessToken(JSON.stringify(user));
    window.sessionStorage.setItem("role", tokenInfo.role);
    window.sessionStorage.setItem("uuid", tokenInfo.uuid);
  }

  public getRole(): any {
    return window.sessionStorage.getItem("role");
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);

    if (user) {
      let tokenInfo = this.getDecodedAccessToken(JSON.stringify(user));
      return tokenInfo;
    }

    return {};
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (e) {
      return null;
    }
  }
}