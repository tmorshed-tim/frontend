import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const URL_API = 'http://localhost:8000/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }),
  
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    const body = `grant_type=&username=${username}&password=${password}&client_id=&client_secret=`;
    return this.http.post(URL_API + 'login', body, httpOptions);
  }

  register(login: string, email: string, password: string, name: string, address: string, role: string): Observable<any> {
    return this.http.post(URL_API + 'user', {
      login,
      email,
      password,
      name,
      address,
      role
    }, httpOptions);
  }
  
}