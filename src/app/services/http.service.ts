import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TokenStorageService } from '../services/token-storage.service';

const baseUrl = 'http://localhost:8000/';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {}

  /*
  request(method: string, route: string, data?: any) {
    if (method === 'GET') {
      return this.get(route, data);
    }

    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.request(method, baseUrl + route, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: header
    });
  }
*/
  get(route: string, data?: any) {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    let params = new HttpParams();
    if (data !== undefined) {
      Object.getOwnPropertyNames(data).forEach(key => {
        params = params.set(key, data[key]);
      });
    }
    
    return this.http.get(baseUrl + route, {
      responseType: 'json',
      headers: header,
      params
    });
  }
/*
  post(route: string, data?: any) {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    let params = new HttpParams();
    if (data !== undefined) {
      Object.getOwnPropertyNames(data).forEach(key => {
        params = params.set(key, data[key]);
      });
    }

    return this.http.post(baseUrl + route, {
      responseType: 'json',
      headers: header,
      params
    });
  }

  update(route: string, data?: any) {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    let params = new HttpParams();
    if (data !== undefined) {
      Object.getOwnPropertyNames(data).forEach(key => {
        params = params.set(key, data[key]);
      });
    }

    return this.http.put(baseUrl + route, {
      responseType: 'json',
      headers: header,
      params
    });
  }

  delete(route: string, data?: any) {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    let params = new HttpParams();
    if (data !== undefined) {
      Object.getOwnPropertyNames(data).forEach(key => {
        params = params.set(key, data[key]);
      });
    }

    return this.http.delete(baseUrl + route, {
      responseType: 'json',
      headers: header,
      params
    });
  }
  */
}
