import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReckoningsResponse } from '../models/reckonings';

import { TokenStorageService } from '../services/token-storage.service';

const baseUrl = 'http://localhost:8000/reckonings';

@Injectable({
  providedIn: 'root'
})
export class ReckoningsService {

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) { }

  getAll(): Observable<ReckoningsResponse[]> {
    let params = new HttpParams();
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.get<ReckoningsResponse[]>(baseUrl,{
      responseType: 'json',
      headers: header,
      params
    });
  }
  
  get(reckoning_number: any): Observable<ReckoningsResponse> {
    let params = new HttpParams();
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.get(`${baseUrl}/${reckoning_number}`,{
      responseType: 'json',
      headers: header,
      params
    });
  }

  create(data: any): Observable<any> {
    let params = new HttpParams();
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };
    
    return this.http.post(baseUrl, data,{
      responseType: 'json',
      headers: header,
      params
    });
  }

  update(reckoning_number: any, data: any): Observable<any> {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };
    
    return this.http.put(baseUrl, data,{
      responseType: 'json',
      headers: header,
      params: new HttpParams().set('reckoning_number', reckoning_number)
    });
  }


  delete(reckoning_number: any): Observable<any> {
    const header = { Authorization: `Bearer ${this.tokenStorage.getToken()}` };

    return this.http.delete(`${baseUrl}/${reckoning_number}`,{
      responseType: 'json',
      headers: header
    });
  }

}
