import { Component, OnInit } from '@angular/core';
import { ClientsData } from '../models/clients-data';
import { CurrentUser } from '../models/current-user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  currenUser: CurrentUser = {
  name : '',
  address: ''
}

newUser: CurrentUser = {
  name : '',
  address: ''
}

clients?: ClientsData[] = [];

role= '';

sellerCode=null;

isClient= false;
isSeller=false;
isEdited= false;
isClientsList = false;
isZeroClients = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getCurrentUser();
    this.getCurrentRole();
  }

  getCurrentUser(){
    this.userService.get()
      .subscribe(
        data => {
          this.currenUser = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  getCurrentRole(){
    this.role=this.userService.getCurrentRole()

    if(this.role == "Client"){
      this.isClient= true;
      this.isSeller =false;
    }
    else if (this.role == "Seller")
    {
      this.isClient= false;
      this.isSeller =true;
    }
    else
    {
      this.isClient = false;  
    }
  }
  
  editUser(){
    this.isEdited = true;
  }

  showClientsList(){
    this.isClientsList = true;
    this.getClientsList();
  }

  hideClientsList(){
    this.isClientsList = false;
  }

  updateUser(){
    this.isEdited = false;

    if (this.newUser.name == '')
      this.newUser.name = this.currenUser.name

    if (this.newUser.address == '')
      this.newUser.address = this.currenUser.address

    this.userService.update(this.newUser)
      .subscribe(
        response => {
          this.getCurrentUser();
        },
        error => {
          console.log(error);
        });
  }

  connectToSeller(){
    if (this.sellerCode != null)
      this.userService.connectToSeller(this.sellerCode)
        .subscribe(
          response => {
            console.log("Added")
          },
          error => {
            console.log(error);
          });
  }

  getClientsList(){
    this.userService.getClientsList()
      .subscribe(
        data => {
          this.clients = data;
          if (data.length == 0)
            this.isZeroClients = true;
            this.isClientsList = false;
        },
        error => {
          console.log(error);
        });
    
  }
}
