import { Component, OnInit } from '@angular/core';
import { Buyer } from 'src/app/models/buyer';
import { ReckoningsResponse } from 'src/app/models/reckonings';
import { Seller } from 'src/app/models/seller';
import { ReckoningsService } from 'src/app/services/reckonings.service';
import { UserService } from 'src/app/services/user.service';

import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  reckonings?: ReckoningsResponse[] = [];
  currentReckoning: ReckoningsResponse = {};
  currentIndex = -1;
  name = '';

  isSeller = false;
  isClient = false;

  constructor(private reckoningsService: ReckoningsService, userService: UserService) { 
    this.retrieveAllReckonings();

    if(userService.getCurrentRole() == "Client")
      this.isClient = true;
    else if (userService.getCurrentRole() == "Seller")
      this.isSeller=true;
  }

  ngOnInit(): void {



  }

  retrieveAllReckonings(): void {
    this.reckoningsService.getAll()
      .subscribe(
        data => {
          this.reckonings = data;
          console.log(data)
        },
        error => {
          console.log(error);
        });
  }

  deleteReckoning(reckoning_number: string): void {
    this.reckoningsService.delete(reckoning_number)
    .subscribe(
      response => {
        this.refreshList();
      },
      error => {
        console.log(error);
      });

      this.refreshList();
  }

  setActiveReckoning(reckoning: ReckoningsResponse, index: number): void {
    this.currentReckoning = reckoning;
    this.currentIndex = index;
  }

  refreshList(): void {
    this.retrieveAllReckonings();
    this.currentReckoning = {};
    this.currentIndex = -1;
  }
}
