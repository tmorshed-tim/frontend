import { Component, OnInit } from '@angular/core';
import { Buyer } from 'src/app/models/buyer';
import { ReckoningsResponse } from 'src/app/models/reckonings';
import { Seller } from 'src/app/models/seller';
import { ReckoningsService } from 'src/app/services/reckonings.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  seller: Seller = {
    name: '',
    address: ''
  }

  buyer: Buyer = {
    name: '',
    address: '',
    uuid: ''
  }

  reckoning: ReckoningsResponse = {
    number: '',
    buyer: this.buyer,
    seller: this.seller,
    place_of_sale: '',
    date_of_issue: '',
    date_of_payment: '',
    date_sale: '',
    status: '',
    positions: [{
      position_id: 1,
      name: '',
      quantity: 1,
      unit: '',
      price: 2,
      tax: 2
    }],
    comment: '',
    person_issuer: '',
    person_buyer: ''
  };

  constructor(private reckoningsService: ReckoningsService) {  }

  ngOnInit(): void {
  }

  createReckoning(): void
  {
    this.reckoningsService.create(this.reckoning)
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }
}
