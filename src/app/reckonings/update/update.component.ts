import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Buyer } from 'src/app/models/buyer';
import { ReckoningsResponse } from 'src/app/models/reckonings';
import { Seller } from 'src/app/models/seller';
import { ReckoningsService } from 'src/app/services/reckonings.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  reckoning_number = '';

  seller: Seller = {
    name: '',
    address: ''
  }

  buyer: Buyer = {
    name: '',
    address: '',
    uuid: ''
  }

  reckoning: ReckoningsResponse = {
    number: '',
    buyer: this.buyer,
    seller: this.seller,
    place_of_sale: '',
    date_of_issue: '',
    date_of_payment: '',
    date_sale: '',
    status: '',
    positions: [{
      position_id: undefined,
      name: '',
      quantity: undefined,
      unit: '',
      price: undefined,
      tax: undefined
    }],
    comment: '',
    person_issuer: '',
    person_buyer: ''
  };

  constructor(private reckoningsService: ReckoningsService, private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit(): void {
    this.reckoning_number = this.route.snapshot.params['reckoning_number'];
    this.getReckoning();
  }

  getReckoning(){
    this.reckoningsService.get(this.reckoning_number)
      .subscribe(
        data => {
          this.reckoning = data;
          if(this.reckoning.buyer != undefined)
            this.buyer = this.reckoning.buyer;
          if(this.reckoning.seller != undefined)
            this.seller = this.reckoning.seller;
        },
        error => {
          console.log(error);
        });
  }
  
  updateReckoning(){
    this.reckoningsService.update(this.reckoning_number,this.reckoning)
      .subscribe(
        response => {
          console.log(response);
          this.getReckoning();
        },
        error => {
          console.log(error);
        });
  }
}
