import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ContactComponent} from './contact/contact.component';

import {ListComponent} from './reckonings/list/list.component';
import {ShowComponent} from './reckonings/show/show.component';
import {AddComponent} from './reckonings/add/add.component';
import {UpdateComponent} from './reckonings/update/update.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'reckonings/list', component: ListComponent},
  { path: 'reckonings/show/:reckoning_number', component: ShowComponent},
  { path: 'reckonings/add', component: AddComponent},
  { path: 'reckonings/update/:reckoning_number', component: UpdateComponent},
  {path: 'profile', component: UserProfileComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
