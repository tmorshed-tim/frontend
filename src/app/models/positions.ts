export class Positions {
    position_id?: number;
    name?: string;
    quantity?: number;
    unit?: string;
    price?: number;
    tax?: number;
}
