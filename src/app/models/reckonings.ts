import { Buyer } from "./buyer";
import { Positions } from "./positions";
import { Seller } from "./seller";

export class ReckoningsResponse {
    reckoning_id?: number;
    number?: string;
    buyer?: Buyer;
    seller?: Seller;
    place_of_sale?: string;
    date_of_issue?: string;
    date_of_payment?: string;
    date_sale?: string;
    status?: string;
    positions?: Positions[];
    comment?: string;
    person_issuer?: string;
    person_buyer?: string;
}
